Etude virtualisation : Cas du load balancing


Sommaire

A - Introduction
B - Qu’est ce que le Load Balancing ?
C - Impact de la virtualisation sur le load balancing
D - Cas étudié : l’attaque DDOS
E - Applications
        a - AWS Shield
        b - Infrastructure BP7 auto scaling
        c - Infrastructure BP6
        d - Infrastructure BP5
        e - Infrastructure BP4
...
F - Bibliographie


Introduction

Dans cette étude, nous nous sommes intéressés à la virtualisation du load balancing au sein des infrastructures IT des entreprises. Ce domaine étant très vaste, nous avons décidé de rentrer plus en détails en ciblant les attaques DDOS et l’utilisation des différentes solutions de Load Balancing pour les en empêcher.


Qu’est ce que le Load Balancing ?

L’équilibrage des charges ou « Load Balancing » est un composant qui permet de répondre de manière efficace à un fort trafic web, c'est-à-dire à des centaines de milliers, voire des millions de demandes simultanées d'utilisateurs ou de clients. Pour illustrer le fonctionnement, prenons l'exemple d'un serveur web unique : 
s'il tombe en panne ou s'il ne supporte pas la charge soumise par un nombre important de demandes utilisateurs, alors le site Web rencontrera des problèmes de performance ou sera tout simplement inaccessible. De ce fait, le Load Balancing a été mis en place pour répondre à ce problème en répartissant la charge de travail sur plusieurs serveurs appelés “parc de serveurs”.

C'est un équilibreur de charge, jouant le rôle d’un « agent de circulation », qui achemine les demandes des clients sur tous les serveurs capables d'y répondre, en maximisant la vitesse de traitement et l’utilisation de la capacité des serveurs. Ce load balancer a donc la mission de garantir qu'aucun serveur ne soit surchargé, il voit quels sont les serveurs disponibles et dirige la requête vers les serveurs pouvant répondre le plus rapidement possible. Si un seul serveur tombe en panne, il redirige le trafic vers les autres serveurs en ligne.



Impact de la virtualisation sur le load balancing

La virtualisation s’est imposée comme LA technologie inhérente à l’optimisation des ressources informatiques. Quasiment tous les data centers l’ont adopté, ce qui ouvre de nouvelles perspectives quant à l’adaptation des solutions périphériques aux architectures virtualisées. Au fil du temps, les applications physiques tendent à se décliner sous forme virtuelle notamment grâce aux nombreux avantages qu’offre la virtualisation : en effet, le déploiement et l’administration des serveurs s’en trouvent grandement simplifiés. La virtualisation est ainsi devenue progressivement une technologie basique et un support des infrastructures cloud.

Un répartiteur de charge devient quasiment obligatoire au sein des infrastructures IT, qu'il soit virtuel ou physique. Dans le cas d’une plateforme physique, un serveur fera office de load balancer, engendrant ainsi un certain coût pour l'entreprise. Dans le cas de la virtualisation, un logiciel de load balancing peut être directement installé sur une machine virtuelle sous forme d'application, il sera plus flexible et permettra à son bénéficiaire de faire évoluer la charge automatiquement, en fonction des prévisions de trafic.

Cas étudié : l’attaque DDOS 

Le principe d'une attaque DDOS est de rendre, pendant une certaine durée suivant l'intensité de l'attaque, inopérant des services en ligne en surchargeant le trafic avec des messages, des demandes de connexion, de faux paquets. Il existe plusieurs modèles d'attaques DDOS suivant les couches visées :

3ème couche : Smurf Attack, ICMP Floods
4ème couche : SYN Floods, UDP Floods
5ème couche : HTTP-encrypted attack

Même si la philosophie de l'attaque semble simple, elle demande néanmoins un vaste réseau d'ordinateurs infectés prêts à envoyer des paquets. Les ordinateurs infectés s'appellent des zombies ou “botnet” car ils appartiennent à ce qu'on appelle un réseau de bot. Il est par ailleurs légitime de comparer le réseau de botnet à un Cloud dans l'aspect où les hackers utilisent les ressources de ce réseau pour répondre à leurs besoins.

Applications

AWS Shield

AWS Shield est un service de protection contre les attaques DDOS (Distributed Denial of Service) pour les applications tournant sur AWS. Le service AWS Shield permet une constante gestion du trafic permettant de diminuer les temps d'arrêts mais aussi la latence des applications, tout en offrant une protection contre les attaques connues des couches 3 et 4.



Infrastructure BP7 l'auto scaling

Un moyen de minimiser l'impact des attaques DDOS sur la qualité de service des serveurs est d'augmenter les ressources du Cloud. Dans le cas de toute application web, il est possible d'utiliser un système de load balancer (précédemment décrit) afin de mieux répartir les demandes sur l'instance d'Amazon EC2 (Elastic Compute Cloud). Ces instances permettent de gérer les rapides montées de demandes incluant les attaques DDOS. Cette approche protège la disponibilité des services du Cloud lorsqu'une quantité importante de requêtes arrive. Il est aussi possible d'être protégé par les attaques de type TLS abuse.

Infrastructure BP6

Une attaque DDOS suffisamment importante peut surcharger les capacités d'encaissage d'une seule instance d'Amazon EC2. Cependant, avec le service Elastic Load Balancing (ELB), il est possible de diminuer la charge des instances en la distribuant parmi les instances disponibles. Pour toute application développée au sein d'Amazon VPC (Amazon Virtual Private Cloud), il y a 4 types d'ELB : 

Classic Load Balancer : Ce service travaille spécifiquement sur les caractéristiques de la couche 7 tel que le X-Forwarded ou les sticky sessions. Mais aussi sur la couche 4 lorsqu'il s'agit purement d'établir des liaisons avec le protocole TCP.
Gateway Load Balancing : équilibre les charges au niveau de la passerelle de couche 3 en plus d'un équilibrage de la couche 4. Elle est équipée notamment d'un panneau de contrôle, d'une gestion automatique des instances suivant la quantité de ressource demandée.
Network Load Balancing : opère intégralement pour la couche 4, ce ELB est optimisé pour encaisser des charges très importantes de requêtes volatiles ou non sur les trafics TCP ou UDP.
Application Load Balancer : travail exclusivement avec la couche 7, elle permet de :
Faciliter la mise en place d'un firewall avec le service AWS WAF
Établir des redirections
Utiliser l'extension du protocole TLS : SNI (Server Name Indication)
Mettre en place de la conteneurisation des applications

Infrastructure BP5

Les infrastructures BP6 et BP7  sont regroupées dans celui du BP5 nommé VPC pour Virtual Private Cloud. Cette infrastructure fait office de couche gestion de droit et d'autorisation d'accès, permettant de d'établir des redirections et de minimiser les impacts des attaques. Par exemple : Il est possible d'établir une règle autorisant les requêtes d'internet à accéder au ELB security group (fonctionnant comme un firewall pour les instances ELB), et une autre règle permettant ensuite aux requêtes de ELB security group d’accéder à l'application web.

Infrastructure BP4








































Bibliographie 

https://searchdatacenter.techtarget.com/feature/Modern-load-balancer-options-reflect-the-virtualized-environment

https://aws.amazon.com/fr/elasticloadbalancing/application-load-balancer/

https://docs.aws.amazon.com/fr_fr/elasticloadbalancing/latest/application/introduction.html

https://docs.aws.amazon.com/fr_fr/elasticloadbalancing/latest/classic/elb-security-groups.html

https://devcentral.f5.com/s/articles/back-to-basics-load-balancing-virtualized-applications

https://d0.awsstatic.com/whitepapers/Security/DDoS_White_Paper.pdf

https://d1.awsstatic.com/whitepapers/aws-overview.pdf

https://d1.awsstatic.com/whitepapers/aws-amazon-vpc-connectivity-options.pdf

https://www.silicon.fr/exceliance-le-load-balancing-virtuel-dans-le-nuage-71152.html

https://www.networkcomputing.com/data-centers/server-virtualization-30-and-load-balancing






